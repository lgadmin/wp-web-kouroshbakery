(function($) {

    //=require components/helper.js 
    $(document).ready(function(){

        //=require window-event/ready.js 
        
    });

    $(window).on('load', function(){

        //=require window-event/load.js 

    });

    $(window).on('scroll', function(){

        //=require window-event/scroll.js 

    })

    $(window).on('resize', function(){

        //=require window-event/resize.js 

    })

}(jQuery));