// Windows Ready Handler

//=require ../components/nav.js 

repositionStickyHeader();

$('.learn-more').on('click', function(e){
	
	e.preventDefault();

	var scrollTo = $(this).attr('href');
	var settings = {
        duration: 2 * 1000,
        offset: ($('.site-header').outerHeight()) * -1
    };

    headerScrollTo(scrollTo, settings);
});