<?php
/**
***	Home
**/

get_header(); ?>

		<div id="primary">
			<div id="content" role="main" class="site-content full-width">

			<!-- Slider -->
			<?php
				$slider = get_field('slider_shortcode');

				if($slider){
					echo do_shortcode($slider);
				}
			?>
			<!-- end Slider -->

			<!-- CTA -->
			<?php
				$cta_title = get_field('cta_title');
				$cta_button_text = get_field('cta_button_text');
				$cta_link_to = get_field('cta_link_to');
			?>
			<div class="site-cta block">
				<div class="container">
					<div class="copy">
						<h1 class="title h2"><?php echo $cta_title; ?></h2>
					</div>
					<div class="cta-button">
						<a href="<?php echo $cta_link_to; ?>" class="cta learn-more"><?php echo $cta_button_text; ?></a>
					</div>
				</div>
			</div>
			<!-- end CTA -->

			<!-- About -->
			<?php
				$about_title = get_field('about_title');
				$about_description = get_field('about_description');
				$about_image = get_field('about_image');
			?>

			<div id="about">
				<div class="container split-content reverse align-top">
					<div class="half-image">
						<img src="<?php echo $about_image['url']; ?>" alt="<?php echo $about_image['alt']; ?>">
					</div>
					<div class="half-copy">
						<h2><?php echo $about_title; ?></h2>
						<?php echo $about_description; ?>

						<?php get_template_part("/templates/template-parts/contact-info"); ?>
					</div>
				</div>
			</div>
			<!-- end About -->

			<!-- Responsibility -->
			<?php
				$responsibility_title = get_field('responsibility_title');
				$responsibility_image = get_field('responsibility_image');
			?>
			<div id="responsibility">
				<div class="container split-content align-top">
					<div class="half-image">
						<img src="<?php echo $responsibility_image['url']; ?>" alt="<?php echo $responsibility_image['alt']; ?>">
					</div>
					<div class="half-copy">
						<?php if($responsibility_title): ?>
							<h2 class="h2"><?php echo $responsibility_title; ?></h2>
						<?php endif; ?>
						<?php
							if( have_rows('responsibility_list') ):
								?>		
								<ul class="responsibility-list">
								<?php
							    while ( have_rows('responsibility_list') ) : the_row();
							        $item = get_sub_field('item');
							        ?>
									<li><i class="fa fa-check" aria-hidden="true"></i><span><?php echo $item; ?></span></li>
							        <?php
							    endwhile;
							    ?>
							    </ul>
							    <?php
							else :
							    // no rows found
							endif;
						?>
					</div>
				</div>
			</div>
			<!-- end Responsibility -->

			<!-- Service -->
			<?php
				$services_title = get_field('services_title');
				$services_description = get_field('services_description');
				$services_background_image = get_field('services_background_image');
			?>

			<div id="services" class="block center" style="background-image: url('<?php echo $services_background_image; ?>');">
				<div class="container">
					<h2 class="h2"><?php echo $services_title; ?></h2>
					<?php echo $services_description; ?>

					<?php
						if( have_rows('services_list') ):
							?>		
							<ul class="service-list">
							<?php
						    while ( have_rows('services_list') ) : the_row();
						        $icon = get_sub_field('icon');
						        $title = get_sub_field('title');
						        $description = get_sub_field('description');
						        $link_to = get_sub_field('link_to');
						        ?>
								<li>
									<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" class="service-icon">
									<h3 class="h2"><?php echo $title ;?></h3>
									<?php echo $description; ?>
									<a href="<?php echo $link_to; ?>" class="learn-more">Learn more</a>
								</li>
						        <?php
						    endwhile;
						    ?>
						    </ul>
						    <?php
						else :
						    // no rows found
						endif;
					?>
				</div>
			</div>

			<!-- end Service -->

			<!-- Testimonial -->
			<div id="testimonial" class="block">
				<div class="container">
					<?php echo do_shortcode('[lg-testimonial]'); ?>
				</div>
			</div>
			<!-- end Testimonial -->

			<!-- Properties -->
			<!--<?php
				$featured_properties_background_image = get_field('featured_properties_background_image');
				$featured_properties_title = get_field('featured_properties_title');
				$featured_properties_description = get_field('featured_properties_description');
			?>

			<div id="properties" class="block center" style="background-image: url('<?php echo $featured_properties_background_image; ?>');">
				<div class="container">
					<h2 class="h2"><?php echo $featured_properties_title; ?></h2>
					<?php echo $featured_properties_description; ?>

					<?php
						if( have_rows('featured_properties_list') ):
							?>
							<ul class="property-list">
							<?php
						    while ( have_rows('featured_properties_list') ) : the_row();
						        $thumbnail = get_sub_field('thumbnail');
						        $address = get_sub_field('address');
						        $area = get_sub_field('area');
						        ?>
								<li>
									<img src="<?php echo $thumbnail['url']; ?>" alt="<?php echo $thumbnail['alt']; ?>">
									<div>
										<div class="address"><?php echo $address; ?></div>
										<div class="area"><?php echo $area; ?></div>
									</div>
								</li>
						        <?php
						    endwhile;
						    ?>
						    </ul>
						    <?php
						else :
						    // no rows found
						endif;
					?>
				</div>
			</div>-->
			<!-- end Properties -->

			<!-- Location -->
			<?php
				$location_title = get_field('location_title');
				$location_description = get_field('location_description');
				$location_map = get_field('location_map');
			?>

			<div id="location" class="block center">
				<div class="container">
					<h2 class="h2"><?php echo $location_title; ?></h2>
					<p><?php echo $location_description; ?></p>
				</div>
			</div>
			<div class="map">
				<?php echo do_shortcode($location_map); ?>
			</div>

			<!-- end Location -->

			<!-- Connect -->
			<?php
				$connect_title = get_field('connect_title');
				$connect_description = get_field('connect_description');
				$connect_image = get_field('connect_image');
			?>

			<div id="connect" class="block center">
				<div class="container">
					<h2 class="h2"><?php echo $connect_title; ?></h2>
					<?php echo $connect_description; ?>

					<div class="connect-info split-content reverse align-top no-pb no-pt">
						<div class="half-image">
							<img src="<?php echo $connect_image['url']; ?>" alt="<?php echo $connect_image['alt']; ?>">
						</div>
						<div class="half-copy">
							<?php get_template_part("/templates/template-parts/contact-info"); ?>
						</div>
					</div>
				</div>
			</div>
			<!-- end Connect -->

			<!-- Contact -->
			<?php
				$contact_title = get_field('contact_title');
				$form = get_field('form');
			?>
			
			<div id="contact" class="block center">
				<div class="container">
					<h2 class="h2"><?php echo $contact_title; ?></h2>
					<?php echo do_shortcode($form); ?>
				</div>
			</div>
			<!-- end Contact -->

			</div>
		</div>
<?php get_footer(); ?>