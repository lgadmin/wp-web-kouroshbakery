<ul class="contact-info">
	<li>
		<span class="icon"><i class="fa fa-building-o" aria-hidden="true"></i></span> <span><?php echo do_shortcode('[lg-address1]'); ?> <?php echo do_shortcode('[lg-city]'); ?>, <?php echo do_shortcode('[lg-province]'); ?><br><?php echo do_shortcode('[lg-postcode]'); ?> <?php echo do_shortcode('[lg-country]'); ?></span>
	</li>
	<li>
		<span class="icon"><a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></span> <span><a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><?php echo do_shortcode('[lg-email]'); ?></a></span>
	</li>
	<li>
		<span class="icon"><a href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><i class="fa fa-phone" aria-hidden="true"></i></a></span> <span><a href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo do_shortcode('[lg-phone-main]'); ?></a></span>
	</li>
	<li class="social-info">
		<?php echo do_shortcode('[lg-social-media list="google+"]'); ?> <span>Connect with Aggie</span>
	</li>
</ul>