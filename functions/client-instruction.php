<style>
	ul{
		list-style:default;
		padding-left:20px;
	}
</style>

<div>
	<h1>Client Instruction</h1>

	<div style="padding: 15px; background-color:#fff;">
		
		<div>
			<h2>Site General</h2>
			<ul>
				<li><b>Logo:</b> Dashboard -> LG Theme -> Site Info -> Logo (It has to be svg since header and footer share the same logo with different color)</li>
				<li><b>Social Media:</b> Dashboard -> LG Theme -> Social Media</li>
				<li><b>Contact Info:</b> Dashboard-> LG Theme -> Contact</li>
				<li><b>Tracking Scripts:</b> Dashboard -> LG Theme -> Analytics Tracking (Google tag manager setup under Longevity Account, No code injection to the header)</li>
			</ul>
		</div>

		<hr>

		<div>
			<h2>Pages</h2>
			<ul>
				<li><b>Home:</b> Dashboard -> Pages -> Home</li>
			</ul>
		</div>

		<hr>

		<div>
			<h2>Testimonial</h2>
			<ul>
				<li>Dashboard -> LG Components -> Testimonial</li>
				<li>Dashboard -> LG Components -> Testimonial Settings</li>
			</ul>
		</div>

	</div>

</div>