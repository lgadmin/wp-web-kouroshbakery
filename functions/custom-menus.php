<?php

function lg_create_menu() {
    add_menu_page(
        __( 'Aggie Fritz', 'Aggie Fritz' ),     ### Need change
        'Aggie Fritz',        ### Need change
        'manage_options',
        'aggie-fritz',   ### Need change
        'lg_main_menu',
        'dashicons-admin-site',
        2
    );

    add_submenu_page(
		'aggie-fritz',
		__('Client Instruction'), 
		__('Client Instruction'), 
		'edit_themes', 
		'client-instruction', 
		'lg_cms'
	);
}

add_action( 'admin_menu', 'lg_create_menu' );

function lg_main_menu(){
	echo '<div class="wrap"><h1>'. get_bloginfo() .' CMS</h1></div>';
}

function lg_cms(){
	require_once "client-instruction.php";
}

?>