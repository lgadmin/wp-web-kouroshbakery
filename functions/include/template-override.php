<?php

function archive_template( $slugPath, $idPath ){
	$templateRoot = get_stylesheet_directory() . '/templates';
	$slugPath = $templateRoot . $slugPath;

	//Check if id is provided
	if($idPath){
		$idPath = $templateRoot . $idPath;
	}

	if(file_exists($slugPath))
		return $slugPath;
	else if($idPath && file_exists($idPath)){
		return $idPath;
	}

	return false;
}

function lg_single_template( $single_template ) {
    global $post;
    
    $template = archive_template(
    	'/single/'.$post->post_type.'.php',
    	false
    );

    if($template)
    	return $template;
    else
    	return $single_template;
}

function lg_page_template( $page_template ) {
    global $post;
    
    $template = archive_template(
    	'/page/'.$post->post_name.'.php',
    	'/page/'.$post->ID.'.php'
    );

    if($template)
    	return $template;
    else
    	return $page_template;
}

function lg_taxonomy_template( $taxonomy_template){
    $tax = get_queried_object();
    
    $template = archive_template(
    	'/taxonomy/'.$tax->taxonomy.'.php',
    	false
    );

    if($template)
    	return $template;
    else
    	return $taxonomy_template;
}

function lg_archive_template( $archive_template ) {
    $archive = get_queried_object();
    
    $template = archive_template(
    	'/archive/'.$archive->name.'.php',
    	false
    );

    if($template)
    	return $template;
    else
    	return $archive_template;
}

add_filter( 'single_template', 'lg_single_template' ) ;
add_filter( "page_template", "lg_page_template" ); 
add_filter( "taxonomy_template", "lg_taxonomy_template" );
add_filter( 'archive_template', 'lg_archive_template' ) ;

?>